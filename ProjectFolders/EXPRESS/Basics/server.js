const express=require('express');
const cors = require('cors');
const app=express();

app.use(express.json());
app.use(cors());

app.use('/home',require('./home'));


app.use('/fetch', require('./fetch'));
app.use('/login',require('./login'));
// app.use('/register',require('./register'));
app.use('/',require('./update'));
app.use('/Delete',require('./Delete'));


app.use('/register', require('./pro1/register'));
app.use('/login2', require('./pro1/login2'));

app.listen(3003,()=>{
    console.log('I am Running...')
});