const express = require('express');
const app = express();
let fs = require('fs');

let rawData = fs.readFileSync('./employee.json','utf-8')
let data =JSON.parse(rawData);
app.get('/',(req,res)=>{
    res.send("welcome to ExpressJS")
})
app.get('/getAllEmployees',(req,res)=>{
    res.send(data.employees)
})

app.get('/getEmployee/:index',(req,res)=>{
    res.send(data.employees[req.params.index])
})
app.get('/getEmployeeByName/:empName',(req,res)=>{

let employee = data.employees.find(emp => emp.empName === req.params.empName);
    if(employee){
        res.send(employee)
    }else{
        res.send("No record found")
    }
})    
app.get('/greetbyname/:name',(req,res)=>{
    res.send(`<h1>Good Morning--${req.params.name}.</h1>`)
})
app.listen(3000,()=>{
    console.log("Server Running.....")
})
