const express = require('express');
const mongodb = require('mongodb');
let app = express();
app.use(express.json());
let mongoclient = mongodb.MongoClient;

let register = express.Router().post('/', (req,res)=>{
    let person_data = {
        "name":req.body.name,
        "mobile":req.body.mobile,
        "email":req.body.email,
        "password":req.body.password
    }
    mongoclient.connect('mongodb://localhost:27017/DATA', (err,db)=>{
        if(err){
            throw err;
        }else{
            db.collection('Users').insertOne(person_data,(err,result)=>{
                if(err){
                    throw err;
                }else{
                    res.send("Data Inserted Successfully");
                }
            });
        }
    });
});
module.exports = register;